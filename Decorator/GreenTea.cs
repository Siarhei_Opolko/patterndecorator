﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator.Components
{
    public class GreenTea : Beverage
    {
        public GreenTea()
        {
            Description = "Green leaf tea";
        }
        public override int GetCost()
        {
            return 110;
        }
    }
}
