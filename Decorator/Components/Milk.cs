﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator.Components
{
    public class Milk : ComponentDecoratorBase
    {
        private Beverage _beverage;

        public Milk(Beverage beverage)
        {
            _beverage = beverage;
            Description = _beverage.GetDescription() + " with Milk";
        }

        public override int GetCost()
        {
            return _beverage.GetCost() + 30;
        }
    }
}
