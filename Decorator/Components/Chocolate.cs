﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator.Components
{
    public class Chocolate : ComponentDecoratorBase
    {
       private Beverage _beverage;

        public Chocolate(Beverage beverage)
        {
            _beverage = beverage;
            Description = _beverage.GetDescription() + " with Chocolate";
        }

        public override int GetCost()
        {
            return _beverage.GetCost() + 60;
        }
    }
}
