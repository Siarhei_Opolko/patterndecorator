﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator.Components
{
    public class Sugar : ComponentDecoratorBase
    {
        private Beverage _beverage;

        public Sugar(Beverage beverage)
        {
            _beverage = beverage;
            Description = _beverage.GetDescription() + " with Sugar";
        }

        public override int GetCost()
        {
            return _beverage.GetCost() + 10;
        }
    }
}
