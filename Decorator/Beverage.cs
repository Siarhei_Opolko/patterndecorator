﻿using System;

namespace Decorator
{
    public abstract class Beverage
    {
        protected string Description { get; set; }

        public string GetDescription()
        {
            return Description;
        }

        public virtual void PrintInfo()
        {
            Console.WriteLine("Bevrage: {0}; Price {1}", GetDescription(), GetCost());
        }

        public abstract int GetCost();
    }
}
