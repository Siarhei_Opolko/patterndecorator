﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator
{
    public class BlackTea : Beverage
    {
        public BlackTea()
        {
            Description = "Black tea from teabag";
        }

        public override int GetCost()
        {
            return 20;
        }
    }
}
