﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Decorator.Components;

namespace Decorator
{
    class Program
    {
        static void Main(string[] args)
        {
            Beverage espresso = new Espresso();
            Beverage blackTea = new BlackTea();
            Beverage greenTea = new GreenTea();

            espresso.PrintInfo();
            blackTea.PrintInfo();
            greenTea.PrintInfo();

            Beverage capuccino = new Sugar(new Milk(new Espresso()));
            capuccino.PrintInfo();

            Beverage blackTeaWithSugar = new Sugar(new BlackTea());
            blackTeaWithSugar.PrintInfo();

            Beverage blackTeaWithDoubleSugar = new Milk(new Chocolate(new GreenTea()));
            blackTeaWithDoubleSugar.PrintInfo();

            Console.ReadLine();
        }
    }
}
